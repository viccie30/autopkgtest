in-testbed
==========

Scripts in this directory are either copied into the testbed and run
from there, or run directly as shell commands. They should generally only
use commands that are Essential: yes or have had their prerequisites checked.

Scripts in this directory need to be runnable on the oldest distribution
that we support using in the testbed: see debian/README.source for our policy
on this.

For scripts that are used with autopkgtest-virt-chroot, the ChrootRunner
in tests/autopkgtest needs to copy all executables needed by these
scripts into its chroot. Scripts that are only used by containers with an
init system do not need this.

wrapper.sh
----------

Run *COMMAND [ARG...]*, sending its stdout through a pipe to this script's
stdout and *OUTPATH*, and its stderr through a pipe to this script's stderr
and *ERRPATH*. If *COMMAND* leaks background processes that hold the pipes
open, terminate those processes. Exit with the exit status of *COMMAND*.

Use this script to wrap anything that runs arbitrary code from the
packages under test, notably the test itself.

There is no need to use this script to wrap simple commands that don't run
code from the package under test (coreutils, apt-cache, dpkg), but ideally
it should be used to wrap anything that runs maintainer scripts.

Options:

* `--artifacts=PATH`: Use *PATH* as `AUTOPKGTEST_ARTIFACTS`
* `--chdir=PATH`: Change directory to *PATH*
* `--env=VAR=VALUE`: Set an environment variable
* `--make-executable=PATH`: chmod *PATH* +x
* `--source-profile`: Pretend to be a login shell
* `--stdout=OUTPATH`: Copy stdout to *OUTPATH*
* `--stderr=ERRPATH`: Copy stderr to *ERRPATH*
* `--script-pid-file=PATH`: Write this script's pid to *PATH*
* `--tmp=PATH`: Use *PATH* as `AUTOPKGTEST_TMP`
* `--unset-env=VAR`: Unset an environment variable
